var treeData = [
    {
        "name": "Event",
        "parent": "null",
        "_children": [
            {
                "name": "A chemical spill in West Virginia that began at a chemical plant and seeped into the Elk River last Thursday left about 300,000 people without access to drinking water for days and is only now getting resolved.",
                "_children": [
                    {
                        "name": 'Tap water test results in 10 West Virginia homes -- part of a state-commissioned study into whether the water is safe to drink after a chemical spill last month -- will be known in one to three weeks, scientists leading the effort said Friday.',
                        "_children": [
                            {
                                "name": '"You can drink it; you can bathe in it; you can use it how you like." Nine counties in West Virginia are under a state of emergency after a chemical spill contaminated the drinking water.'
                            },
                            {
                                "name": "The West Virginia Department of the Health and Human Resources Bureau for Public Health’s Office of Laboratory Services has initial test results back on the drinking water or what is commonly called “the treated water”.. ...more DHHR today reminded residents about the seriousness of cervical cancer as part of National Cervical Cancer Awareness Month, highlighting the West Virginia Breast and Cervical Cancer Screening Program (WVBCCSP).",
                                "_children": [
                                    {
                                        "name": "Karen Bowling, secretary of the state's Department of Health and Human Resources, said Sunday that more than 1,000 people had called the West Virginia Poison Center, concerned about their exposure to contaminated water."
                                    },
                                    {
                                        "name": 'Asked about the chemical, the CDC issued guidance to state authorities in West Virginia suggesting the water would be safe to drink if samples met the safety standard of 1 part per million -- meaning that there is no more than 1 milligram of the chemical in 1 liter of water.'
                                    }
                                ]
                            },
                            {
                                "name": 'The West Virginia Bureau for Public Health advises, after consultation with the U.S. Centers for Disease Control and Prevention (CDC) this evening, that the CDC recommends—out of an abundance of caution—that pregnant women drink bottled water until there are no longer detectable levels of MCHM in the water distribution system.'
                            },
                            {
                                "name": 'More investigations launched as 180,000 residents still without water In this instance, Angie Roser, executive director of the West Virginia Rivers Coalition, said at least three federal laws, including the Clean Water Act and the Safe Drinking Water Act, should have provided oversight of the chemical tanks, but that the local Department of Environmental Protection hasn\'t been on the site since 1991.'
                            },
                            {
                                "name": 'Former Freedom Industries executive Dennis Farrell pleads guilty to federal pollution violations; is one of last charged in connection with West Virginia chemical spill responsible for leaving 300,000 people without clean tap water for days.'
                            },
                            {
                                "name": 'West Virginia American Water\'s McIntyre had a different take, as evidenced by the company\'s unprecedented stop-use warning: "We don\'t know that the water is not safe, but I can\'t say it is safe." Sandra Fisher heard the sound of running water in her Charleston, West Virginia, home on Monday for the first time in four days after a chemical leak fouled water supplies for hundreds of thousands of people.'
                            },
                            {
                                "name": 'In new guidance issued Wednesday night, West Virginia health officials advised pregnant women to wait to drink tap water until there are no detectable levels of the chemical in it.'
                            }
                        ]
                    },
                    {
                        "name": 'About 300,000 people in the US state of West Virginia have been warned not to drink, bathe in, or wash with tap water after a chemical spill into the nearby Elk river.'
                    },
                    {
                        "name": 'The chemical spill that cut off water to more than 300,000 people in West Virginia for several days has exposed serious defects in state and federal environmental protections that allow many facilities and chemicals to escape scrutiny.'
                    },
                    {
                        "name": "previous advice that it does not anticipate any adverse health effects from The West Virginia Bureau of Public Health received the following statement in an Testing is ongoing and State officials are continuing to work with CDC and other experts to ensure the safety of the water for our Earlier today, the manufacturer reported that another material was part of the chemical release that occurred",
                        "_children": [
                            {
                                "name": 'By Greg Botelho and Stephanie Gallman, CNN West Virginia\'s attorney general and state legislators announced Tuesday that they\'d join those investigating a chemical spill that left hundreds of thousands scrambling for safe water, with one senator promising "there will definitely be a change." "This whole series of events is unacceptable," said Senate Majority Leader John Unger, who will be leading the state legislative probe, echoing many others around West Virginia and elsewhere since the crisis boiled up last Thursday.'
                            },
                            {
                                "name": 'Anchored by Jake Tapper, The Lead airs at 4 p.m. (CNN) - Only about half of the 300,000 people affected by a chemical spill in West Virginia have been cleared to use their tap water again.'
                            }
                        ]
                    },
                    {
                        "name": '“We are so desperate for jobs in West Virginia, we don’t want to do anything that pushes industry out,” said Maya Nye, president of People Concerned About Chemical Safety, a citizens group that formed after a 2008 explosion and fire that killed two workers at the Bayer CropScience plant in Institute, W.Va. State officials on Monday began lifting a ban on using tap water, starting with hospitals and extending slowly by zones to the 300,000 people who have been without drinking water since late Thursday.'
                    },
                    {
                        "name": 'She told The New York Times that, "We need to look at our entire system and give some serious thought to making some serious reform and valuing our natural resources over industry interests." She and others also are asking the tough questions: Why was a chemical storage tank allowed to be on a river that\'s used for drinking water? And why wasn\'t the tank inspected since 1991? Then there\'s Ken Ward Jr., from The Charleston Gazette, who reported that the U.S. Chemical Safety Board had recommended three years ago that West Virginia "create a new program to prevent hazardous chemical accidents." Let\'s join Rosser and Ward in their search for answers.'
                    },
                    {
                        "name": 'Days after they told some West Virginia residents they shouldn\'t worry about drinking tap water contaminated with a chemical used to clean coal, local health officials issued a new advisory this week.'
                    },
                    {
                        "name": 'Water restrictions were imposed Thursday after it was discovered that about 7,500 gallons of a chemical used to clean coal - 4-methylcyclohexane methanol - had leaked out of a storage tank a mile upriver from the West Virginia American Water plant.'
                    },
                    {
                        "name": 'While authorities say they\'re detecting little trace of the contaminant in the water, the process of restoring normal service to all of the people in nine West Virginia counties affected by a Thursday chemical spill is moving slowly, and the majority of residents affected still don\'t have water, five days after the leak.'
                    }
                ]
            },
            {
                "name": 'Editorial contends chemical spill in West Virginia has exposed serious defects in state and federal environmental protections that allow many facilities and chemicals to escape scrutiny; maintains that passing of the crisis, as well as fact that residents can resume drinking tap water, should not dissuade the state or the federal government from strengthening and enforcing statutes.',
                "_children": [
                    {
                        "name": 'Federal grand jury indicts four owners and operators of Freedom Industries, which contaminated Elk River in West Virginia with toxic chemical spill in January; spill caused extended cutoff of drinking water to almost 300,000 residents in and near Charleston.',
                        "_children": [
                            {
                                "name": 'Earl Ray Tomblin, the water company and state health officials have all assured residents in recent days that once the leaked chemical, known as MCHM, became diluted to less than one part per million in the drinking water, safety standards set by the Environmental Protection Agency and other agencies would be met.'
                            }
                        ]
                    },
                    {
                        "name": 'A toxicologist with Freedom Industries told the water company that there is "some health risk" with this chemical, according to Laura Jordan of West Virginia American Water.'
                    },
                    {
                        "name": 'The effort -- called the West Virginia Testing Assessment Project -- comes after the chemical 4-methylcyclohexane methanol, or MCHM, was discovered leaking from a storage tank into the Elk River and into Charleston\'s water supply last month.'
                    },
                    {
                        "name": 'In a statement, Alpha noted that water safety in industrial operations had been at the forefront of public attention in recent months with a chemical spill in West Virginia and a coal ash spill in North Carolina'
                    }
                ]
            },
            {
                "name": 'West Virginia is recovering from another hazardous health crisis -- a toxic chemical spilled into the Elk River that poisoned the water supply for hundreds of thousands of residents.'
            },
            {
                "name": 'West Virginia authorities have begun easing a ban on drinking tap water enacted after a chemical spill tainted much of the state\'s water supply.'
            },
            {
                "name": "Weeks after health authorities had told West Virginians that their water was safe to drink again following a toxic spill, schools in Charleston sent students home abruptly last week when students and staff members detected the telltale licorice odor of the leaked chemical.",
                "_children": [
                    {
                        "name": 'When asked why, he said: "Well, because it -- nobody has said that it\'s safe." Spill spews tons of coal ash into North Carolina river Updated 11:14 AM ET, Thu February 20, 2014 Health officials say the tap water near Charleston, West Virginia, is all right to drink.'
                    },
                    {
                        "name": 'And the decades-old chemical storage tank in West Virginia that leaked as much as 10,000 gallons of chemicals used in coal processing into the nearby Elk River, contaminating the water supply of some 300,000 Charleston-area residents, would have been moved and replaced by modern, anti-leak storage tanks and safer containment.'
                    }
                ]
            }
        ]
    }
];

