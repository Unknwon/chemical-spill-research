package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/Unknwon/log"
)

func main() {
	keyword := flag.String("keyword", "keyword", "filter keyword")
	flag.Parse()

	dirName := "clean"
	dir, err := os.Open(dirName)
	if err != nil {
		log.Fatal("Fail to open directory: %v", dirName)
	}

	files, err := dir.Readdir(0)
	if err != nil {
		log.Fatal("Fail to get directory info: %v", dirName)
	}

	for _, f := range files {
		if f.IsDir() {
			continue
		}

		data, err := ioutil.ReadFile(path.Join(dirName, f.Name()))
		if err != nil {
			log.Fatal("Fail to read file: %v", f.Name())
		}
		lines := strings.Split(string(data), "\n")
		for i := range lines {
			if strings.Index(strings.ToLower(lines[i]), *keyword) == -1 {
				continue
			}
			fmt.Println("->", lines[i])
			fmt.Println()
		}
	}
}
