package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"github.com/Unknwon/com"
	"github.com/Unknwon/log"
)

func getDate(fname string) int {
	fpath := strings.TrimSuffix(path.Join("data", fname), ".txt")
	data, err := ioutil.ReadFile(fpath)
	if err != nil {
		log.Fatal("Fail to read file: %v", err)
	}
	content := string(data)

	switch {
	case strings.Contains(fname, "nytimes.com_"):
		anchor := `<meta name="pdate" content="`
		i := strings.Index(content, anchor)
		if i == -1 {
			log.Warn("Can't find date: %s", fpath)
			return 0
		}
		return com.StrTo(content[i+len(anchor) : i+len(anchor)+8]).MustInt()

	case strings.Contains(fname, "bbc.com_"):
		anchor := `"datePublished": "`
		i := strings.Index(content, anchor)
		if i == -1 {
			log.Warn("Can't find date: %s", fpath)
			return 0
		}
		return com.StrTo(strings.Replace(content[i+len(anchor):i+len(anchor)+10], "-", "", 2)).MustInt()

	case strings.Contains(fname, "cnn.com_"):
		anchor := `datePublished" content="`
		i := strings.Index(content, anchor)
		if i == -1 {
			log.Warn("Can't find date: %s", fpath)
			return 0
		}
		return com.StrTo(strings.Replace(content[i+len(anchor):i+len(anchor)+10], "-", "", 2)).MustInt()

	case strings.Contains(fname, "dhhr.wv.gov_"):

	case strings.Contains(fname, "fox.com_"):

	}
	return 0
}

func main() {
	min := flag.Int("min", 40, "minimum characters")
	flag.Parse()

	dirName := "ss"
	dir, err := os.Open(dirName)
	if err != nil {
		log.Fatal("Fail to open directory: %v", dirName)
	}

	files, err := dir.Readdir(0)
	if err != nil {
		log.Fatal("Fail to get directory info: %v", dirName)
	}

	fw, err := os.Create("combine.txt")
	if err != nil {
		log.Fatal("Fail to create combine.txt: %v", err)
	}
	defer fw.Close()

	for _, f := range files {
		if f.IsDir() || f.Name() == ".DS_Store" {
			continue
		}
		fmt.Println(f.Name())

		date := getDate(f.Name())
		dateStr := com.ToStr(date)
		if date == 0 {
			dateStr = "00000000"
		}

		data, err := ioutil.ReadFile(path.Join(dirName, f.Name()))
		if err != nil {
			log.Fatal("Fail to read file: %v", err)
		}
		lines := strings.Split(string(data), "\n")

		prefix := "[" + dateStr + "] "
		for i := range lines {
			// Discard lines with too less characters.
			if len(lines[i]) < *min {
				continue
			}

			fw.WriteString(prefix)
			fw.WriteString(strings.TrimSpace(lines[i]))
			fw.WriteString("\n")
		}
		fmt.Println(date)
	}
}
