© 2000-2015 Fox Television Stations, Inc. All Rights Reserved.
Privacy Policy Terms of Service Ad Choices
Posted:Sep 24 2015 10:21PM EDT
Updated:Sep 25 2015 06:19AM EDT
LUKE, Md. (AP) — According to the Maryland Department of the Environment, 10,000 gallons of latex used for paper coating spilled from the Verso paper mill into the North Branch of the Potomac River during a four-hour period.
The Cumberland Times-News reports (http://bit.ly/1OWvO2J ) that the latex spilled as it was unloaded from a rail car Wednesday, and the spill was discharged through a collection system to the UPRC plant.
MDE spokesman Jay Apperson said the MDE sent an investigator to the site and contacted the Verso paper mill and the Upper Potomac River Commission plant that treats wastewater from the paper mill.
Apperson said, "Although we have no indication of any threat to drinking water facilities... MDE has notified the Interstate Commission on the Potomac River Basin for notification to drinking water facilities not only in Maryland but in Virginia and West Virginia."