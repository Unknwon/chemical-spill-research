Water tests after a chemical spill in West Virginia are encouraging, the governor said, but it's unclear when people might be able to use their taps again.
About 300,000 residents in nine counties in the southwest section of the state can't use tap water.
"Our team has been diligent in testing samples from throughout the affected area. The numbers look good and, like last night, they are very encouraging. I believe that we're at a point where we can say that we see light at the end of the tunnel," West Virginia Gov. Earl Ray Tomblin told reporters Sunday.
Jeff McIntyre, president of West Virginia American Water - a company affected by the spill - said that officials will begin lifting the water bans by zone. Certain areas will be prioritized, including downtown Charleston, but decisions will also depend on test results.
He declined to put a timeline on when the do-not-use orders will be lifted.
"I don't believe we're several days from starting to lift, but I'm not saying today," McIntyre said.
Officials have warned water customers to watch for symptoms of exposure to the chemical, which is used to clean coal, such as skin irritation, nausea, vomiting or wheezing.
Karen Bowling, secretary of the state's Department of Health and Human Resources, said Sunday that more than 1,000 people had called the West Virginia Poison Center, concerned about their exposure to contaminated water. There have also been more than 60 animal exposures reported.
A total of 10 people have been admitted to three hospitals, none in serious or critical condition, and 169 patients have been treated and released from emergency rooms, Bowling said.
Water restrictions were imposed Thursday after it was discovered that about 7,500 gallons of a chemical used to clean coal - 4-methylcyclohexane methanol - had leaked out of a storage tank a mile upriver from the West Virginia American Water plant.
Residents were told to use bottled water to wash hands, brush teeth or take showers.
The federal Department of Homeland Security sent 16 tractor-trailer loads of bottled water to help and the water company also provided truckloads.
The medical impact was hard to assess.
"We've had a lot of worried-well calls," Dr. Rahul Gupta of the Kanawha-Charleston Health Department said over the weekend. He cited complaints of irritation of the skin, throat, chest and stomach that some residents have linked to possible exposure.
The unknowns made residents anxious.
"They don't even know what the health risks are," Stacy Kirk of Culloden told CNN affiliate WSAZ. "We had bathed, cooked and everything right before the news came on yesterday."
"I don't know anything about the chemical to say too much good or bad about it, so we're all up in the air," said Arthur Taylor. "We're common folks - we're not chemists."
Good morning. Excellent report about the ongoing water issue this morning. And thanks for your comment about outrage. You, of course, could not be more correct. One item that will probably help is to have contact info handy so here are the two I found on the W.V. EPA site (please verify according to your due-diligence policies of course). Cristina Schulingkamp: schulingkamp.cristina@epa.gov; Michelle Moyer: moyer.michelle@epa.gov though I’m sure they’ll get letters to the right people of not they.
I’m especially glad you brought up the issue of inspection/lack thereof. I told you I had a story; here it is. During college I worked in the summers and found a job at a plywood manufacturing plant (Marion North Carolina). I was in charge of cleaning the glue machine, glue tower, and glue pit – where I no doubt lost a lot brain cells I couldn’t afford to lose.
When I was finished cleaning the pit, by scraping it down and filling it with water, I pumped the slurry into a dumpster and was told to dump it at the back of the plant by a stream. Don’t ask me what I was thinking but it literally never occurred to me a company would be so irresponsible as to dump something toxic near a stream. Maybe it was the loss of brain cells but I never questioned it.
20 years later I woke in the middle of the night and sat straight up in bed; ‘oh my god…was that toxic waste’? The next morning I called the local EPA office and told the inspector what had happened. When I finished he said, ‘yeah, well, we took care of that last year’ (shocking but at least he was honest). ‘Well, was that substance toxic?’ ‘What do you think’? ‘Are you seriously telling me it took you 20 years to stop it’? He replied, ‘are you seriously telling me it took you 20 years to call it in’ (verbatim)? ‘Are you honestly blaming me?!’ he hung up.
These are just the stories we know. Can you imagine how many more there are? Can you imagine how much toxic water people are drinking? There’s another ongoing story here in Asheville having to do with Duke Power and the French Broad River.
But I’m astounded that issues like these even have to be administrated? I know there are people, who run companies, no doubt in equal percentage to the rest of the population, with impaired intellects, lacking conscience, or have no moral fiber but there are also a whole lot of us who don’t. What is wrong with us that we are so apathetic? That’s a study to itself.
What’s truly shocking is that the same people charged with the responsibility of, and paid to, protect us are drinking the same water…have children who are. I’m not a radical but we should all at least act radically when it comes to basic issues like what we eat, breathe, or drink. Apathy is unfortunately part of human nature but there are times to rise to an occasion and these are certainly some of those.
CNN welcomes a lively and courteous discussion as long as you follow the Rules of Conduct set forth in our Terms of Service. Comments are not pre-screened before they post. You agree that anything you post may be used, along with your name and profile picture, in accordance with our Privacy Policy and the license you have granted pursuant to our Terms of Service.



Catch New Day from 6-9am ET
About the Show
Around the World
Award of the Day
Daily Wrap-up
Exclusives
Five Things to Know For Your New Day
Fun
The Good Stuff
Headlines
Health
Interviews
Midday
Money Time
New Day Weekend
News
Political Gut Check
Social
Tech
Get every new post delivered to your Inbox.

Join 420 other followers

