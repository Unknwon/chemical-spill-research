Nearly 200,000 people in West Virginia awoke Friday to stark warnings about their tap water: Don't drink it. Don't cook with it. Don't even brush your teeth or take a shower in it.
The reason: a chemical spill in the Elk River in the central and southwestern parts of the state.
The news sent shock waves through the region.
"The emergency rooms became packed once the order came out. People had been drinking and bathing in this stuff," said Kent Carper, president of the Kanawha County Commission. "Then there was a run on water at every Walmart and convenience store in the county."
Gov. Earl Ray Tomblin declared a state of emergency Thursday evening for nine countries. The White House issued an emergency declaration, too.
"Right now, our priorities are our hospitals, nursing homes and schools," the governor said. "I've been working with our National Guard and Office of Emergency Services in an effort to provide water and supplies through the county emergency services offices as quickly as possible."
The declaration affects West Virginia American Water Co. customers in Boone, Cabell, Clay, Jackson, Kanawha, Lincoln, Logan, Putnam and Roane counties.
The company said on its Facebook page that a chemical spill occurred along the Elk River, causing contamination within the Kanawha Valley water system.
The chemical, 4-Methylcyclohexane Methanol, is not toxic but is harmful if swallowed, according to Thomas Aluise, a spokesman for the state's Department of Environmental Protection. It is used to wash coal before it goes to market.
The leak came from a 48,000-gallon tank at Freedom Industries, a chemical storage facility about a mile upriver from the West Virginia American Water Co. facility, officials said. It's uncertain how much of the chemical leaked.
A toxicologist with Freedom Industries told the water company that there is "some health risk" with this chemical, according to Laura Jordan of West Virginia American Water.
"The safety sheet indicated there could be some skin or eye irritation if you come in contact, or possibly harmful if swallowed, but that's at full strength of the chemical," Jordan said. "The chemical was diluted in the river."
The do-not-drink advisory was issued as a precaution.
Officials weren't sure when the pipes would be cleared and the water safe to drink again. There's lots of plumbing in the nine-county area.
"You've got 60 miles of this system, and it's full of this water," Carper said. "And people aren't using the water."
CNN welcomes a lively and courteous discussion as long as you follow the Rules of Conduct set forth in our Terms of Service. Comments are not pre-screened before they post. You agree that anything you post may be used, along with your name and profile picture, in accordance with our Privacy Policy and the license you have granted pursuant to our Terms of Service.



Catch New Day from 6-9am ET
About the Show
Around the World
Award of the Day
Daily Wrap-up
Exclusives
Five Things to Know For Your New Day
Fun
The Good Stuff
Headlines
Health
Interviews
Midday
Money Time
New Day Weekend
News
Political Gut Check
Social
Tech
Get every new post delivered to your Inbox.

Join 420 other followers

