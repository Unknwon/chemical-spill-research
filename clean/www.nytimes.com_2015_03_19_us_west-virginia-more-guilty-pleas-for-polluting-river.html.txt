Advertisement
Advertisement
By THE ASSOCIATED PRESSMARCH 18, 2015
Advertisement
Two former employees at Freedom Industries pleaded guilty to a pollution charge Wednesday in a chemical spill that fouled a tap water supply last year. Michael Burdette, the former plant manager, and Robert Reynolds, an environmental consultant, each face a year in prison and a minimum $2,500 fine when they are sentenced on June 24. The spill of thousands of gallons of a coal-cleaning agent from Freedom Industries into the Elk River went into West Virginia American Water’s intake two miles downstream on Jan. 9, 2014. It prompted a tap water ban for 300,000 residents for up to 10 days while the water system was flushed out. Two former owners of the company pleaded guilty this week. Dennis Farrell, also a former owner, and a former company president, Gary Southern, face trial later this year.
A version of this brief appears in print on March 19, 2015, on page A18 of the New York edition with the headline: West Virginia: More Guilty Pleas for Polluting River.  Order Reprints| Today's Paper|Subscribe
Go to Home Page »