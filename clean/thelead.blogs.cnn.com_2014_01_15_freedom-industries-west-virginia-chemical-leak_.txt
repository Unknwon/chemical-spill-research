Anchored by  Jake Tapper, The Lead airs at 4 p.m. ET on CNN.
We've moved! Come join us at our new show page.
(CNN) - Only about half of the 300,000 people affected by a chemical spill in West Virginia have been cleared to use their tap water again. For the others, same deal as the last six days: No drinking it, no bathing in it.
Last week, an estimated 7,500 gallons of a chemical used in coal production leaked into the water supply from Freedom Industries. The leak was first discovered by residents, who noticed an odd licorice-like smell, which authorities traced to the chemical leak from a 35,000-gallon storage tank along the Elk River.

Freedom Industries has been awfully tight-lipped, and when the man identified as the company's president gave a news conference on the leak Friday, he couldn't end it fast enough.
Charleston Daily Mail reported Wednesday that it is no wonder the spill happened, because the company had a number of violations. But they weren't found until after the fact.
Charleston Daily Mail Capitol bureau chief David Boucher joined CNN's "The Lead with Jake Tapper" to discuss.
Comments are closed.
The Lead with Jake Tapper draws not only on Tapper’s deep knowledge of politics and national issues, but also seeks to examine and advance stories across a wide range of topics that demonstrate his own curiosities and interests. Compelling headlines come from around the country and the globe, from politics to money, sports to popular culture, based on news drivers of the day.
The Lead with Jake Tapper airs weekdays at 4 p.m. ET.
Get every new post delivered to your Inbox.

Join 140 other followers

