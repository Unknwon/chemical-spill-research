Advertisement
Teaching ideas based on New York Times content.
We travel on it, feed it to our crops, swim in it, pollute it and, yes, drink it. In some parts of the world, countries even fight over it. Water is everywhere, and yet in many places there’s not nearly enough of it to go around.
In this interdisciplinary lesson, we explore some of the ways in which humanity is learning to cope with shortages or threats to this vital element of life. We also consider ways in which water enriches our life or helps define our interactions. With each topic below, we offer teaching ideas ranging from diagraming where and how we use water in our bodies to gathering policy solutions and raising awareness about water-related issues.

Ways to Think About Water
1. Surviving the Drought, at Home and Abroad
From California to the  Rocky Mountains, Kansas and Texas, water shortages are creating local challenges and even crises. Residents have responded in a variety of ways, from  banning lawns to melting snow in the mountains. But many countries are facing similar problems. In China, officials have recently embarked upon an ambitious engineering project to divert 45 cubic kilometers of water annually from the south to the parched north. Iran has seen its largest lake nearly disappear, leaving ships stranded in the mud. In Morocco, residents of a remote village have occupied a hilltop to protect their water supply from silver mining and pollution.
Survey these stories and make a list of the ways people have reacted to threats against their water supply. How has life changed, and what are they doing about it? Then consider whether actions being taken in one place might be useful somewhere else.
2. Source of Conflict
Drought played a significant role in fueling the war in Syria, after the government allowed commercial farmers to consume as much water as they wanted at the expense of rural farmers. It has also reduced the flow of the Jordan River by more than 90 percent, raising tensions between social groups. And conflict between Israel and the Palestinians has led to the collapse of the sewer system in the densely populated Gaza Strip, threatening water supplies and health on both sides. Still, there are signs of promise with a recent water agreement, and some observers believe Israeli expertise in water management could help resolve regional tensions.
Based on these stories, what seem to be the largest causes of water shortage and conflict in the Middle East? Which of the potential solutions under discussion could go furthest in addressing the problem? Design a mock international conference over the regional water crisis, with representatives from each national and social group as well as farmers and scientists. Ask each student or group to develop a position paper outlining complaints and proposals. Then encourage participants to seek allies and try to develop a consensus for action.
3. Essential but Potentially Harmful
We think of water as a useful substance, but sometimes it carries dangerous toxins and germs, or threatens populations and regions through flooding or the unintended consequences of human plans.  Consider the recent chemical spill in West Virginia; worries over an engineering project in China; and the stench that engulfed Los Angeles because of engineering mistakes made a century earlier.
Can publicity or activism make a difference in such situations, or is it human nature to disregard warnings until it’s too late? Choose and research a local or international situation in which human behavior contributes to a water-related problem. Then develop a publicity strategy — using posters, events, speeches or the Internet – to raise awareness and try to change such behavior. For more teaching ideas related to the West Virginia chemical spill, visit our lesson “Testing the Waters: Investigating Questions About the West Virginia Chemical Spill.”
4. Sometimes a Fancy Drink
Some people take their water seriously. Like the man in Texas who made a name for himself as a water gourmet, or the New York restaurant owners who debate whether to offer customers free water or charge for the bottled stuff. Manhattan has long prided itself on the quality of its tap water, but back in the 1920s, the city was plagued by a taste that some described as fishy or cucumber-like. Even today, people in parts of Texas and Ohio say their local water tastes foul.
How’s the water in your area, and can you tell it apart from store-bought H2O? Check with your local water management authority to see if residents have complained about taste and what they’ve done about it. Hold a taste test, and follow it up with a writing contest for the best-written description or review of each sample. You can search the Internet for glossaries of water-tasters’ terms, or award extra points for the best new words.
5. Endangered Supplies
Where does your area’s water come from, and is it stored safely? Cities and states rushed to add security to their reservoirs following the terrorist attacks of Sept. 11, 2001. More recently, a Times investigation found evidence of harmful pollutants in the rooftop water towers of New York City’s skyline, though officials later said there was no cause for alarm. How much should we worry about the security of our water supplies?
Do research and create a map showing the major sources of drinking water in your area, including rivers and lakes. Then add details to the map showing human habitation; recreational activities; potential sources of pollution; and any security measures or inspection procedures that are in place. Is your area doing enough to protect its supply, and would be there be any potentially negative consequences of adding more security?
New surfaces make water bounce, faster.
6. Good Science
Plenty of scientists devote their careers to studying the “mighty molecule” called water, and the many ways in which it affects human life.  Some focus on how to protect against harm, such as the risk of water that freezes on airplane wings, while others study technologies for cleaning or purifying it for future use.
Research careers that involve water, with each student developing a profile of a scientist and his or her daily activities. Then hold a round of “What’s My Line?” in which students try to match classmates with a list of job titles.
7. A Part of Your Body
More than half of your body’s weight comes from water — and with good reason.  The cytosol, the liquid found inside your body’s cells, is made up of water and dissolved ions, enzymes and small proteins. Your blood — which carries oxygen and food to your cells — is also made up of water, and so is saliva, which helps you digest and swallow food. Water helps lubricate your joints, regulates body temperature and makes up a fluid called lymph, part of your immune system.
Research the path water takes through your body after you drink it, with each student focusing on a different role water plays in the body. Next, draw a large outline of the body on a whiteboard or sheet of poster paper. Have students annotate this drawing with sketches showing how the body uses water.
8.  A Molecular Superhero
Water’s chemical makeup — two hydrogen atoms bonded to an oxygen atom — give it a suite of characteristics not shared by any other liquid. Water molecules are polarized, meaning that one side of the molecule carries a negative electric charge, while the other side is positive. This polarity helps water dissolve many substances, like salt (think of the oceans) or acetic acid, which dissolves in water to become vinegar. Hydrogen bonds between water molecules make water “sticky,” which means that water molecules tend to stick to one another. This stickiness, or cohesion, allows water to travel up the length of a tree, all the way from its roots to its canopy. And fortunately for aquatic and marine life in cold places around the world, water floats when it freezes — meaning fish, frogs and many other animals can survive winter weather in the water beneath the ice.
Research these and other properties of water. Then, write a comic strip or graphic novel featuring water as a superhero.  Or, for a suite of hands-on experiments showing the physical properties of water, check out our lesson “Wondrous Water.”
9. A Business Model
It wasn’t so long ago that international water companies vied to purchase the rights to provide low-income regions of the world with water. That privatization boom appears to have slowed, but there are still plenty of companies that see a profitable future in water.  Some companies focus on a single technology like desalination — removing the salt from sea or brackish water so that it can be used for drinking and agriculture — while other investors aim to boost the entire water industry.
Where do you think the smart money lies for investors in water-related companies, and what are the ethical issues for businesses that deal in one of life’s essential elements? Research publicly-traded companies that manage water (like, for instance, some you might find in a Times search) make some predictions about their stock market performance and then track them over the coming year.
10. A Way to Make a Living
José Leonídio Rosendo dos Santos dives in some of Brazil’s dirtiest rivers for a living.  John Roberts runs New York City’s water and sewer operations. “It’s like a ballet,” Mr. Roberts says, describing the daily challenges of stemming broken water mains from Wall Street to Harlem.
Find someone in your local community — from a carwash operator, ship captain or dog groomer to a manager at a water treatment plant — who makes a living in or around water. Interview that person about his or her experiences managing its wet, slippery, unpredictable tendencies. Or write a fictional story in which water gets the upper hand over one of your characters.
11. A Sign of Change
The drought in the United States has provided no shortage of opportunities for writers to reflect. For some, the human stories of life during a new dust bowl are hard to ignore. Others think of history, politics and the future.
What’s behind recent water shortages and extreme weather events, like the historic storms and flooding in England?  Is global warming to blame, as President Obama and Britain’s prime minister, David Cameron, suggest? Or are other human follies more directly responsible?
Consider the range of water-related problems facing humankind. Then write a carefully reasoned essay in which you suggest the prime factors, citing evidence from The New York Times to support your argument.
12. A Way to Think, Create and Shiver
What happens to your brain when you jump into a pond, river or pool and paddle around? Plenty of people enjoy the meditative powers of a good swim. A lucky few work after-school jobs as mermaids. Then there are the ones who like it cold, leaping into bodies of water in midwinter for the pure joy of it. Timothy Sutton of London said it hurts, but he still appears to like it.
What’s your water story? Write a creative personal essay involving a swim or some other form of aquatic immersion, and don’t spare the adjectives.
This resource may be used to address the academic standards listed below.
7 Knows the physical processes that shape patterns on Earth's surface.
8 Understands the characteristics of ecosystems on Earth's surface Human Systems.
11 Understands the patterns and networks of economic interdependence on Earth's surface.
14 Understands how human actions modify the physical environment.
15 Understands how physical systems affect human systems.
16 Understands the changes that occur in the meaning, use, distribution and importance of resources.
1 Understands atmospheric processes and the water cycle.
6 Understands relationships among organisms and their physical environment.
6 Comments
Comments are moderated. FAQ »


I really dont have a water story. i do love being by it and drinking it though.
Water is like love, tenderness; its tremendous value is specifically recognized when it is lost, or even scant. As pediatric nephrologists, caring  for children at home chronic dialysis,  one of the main goals is for our patients  to have adequate water   management thus avoiding dehydration or water excess edema and hypertension. Treating of these water-related disorders is relatively easy, with current technologies, but the lack of family love is a life threatening condition. No machine even sophisticated technology can offset love lack. Technological and scientific progress without solidarity and  compassionate care is certainly doomed to failure.
Acad. Jose Grunberg . Uruguay.  South America
I am delighted that I stumbled across this “Recommendation”. I only wish that THIS WOULD get the same level of coverage as Climate Change. Clearly, one still sees all of the arguments pro and con with respect to that most important phenomenon. And, for the most past, the negative aspects of the problem are well-stated and generally accepted.
This is good.
But, to me, what is better is to share focus, to give the water problem its fair share of discussion. To me, it is a GREATER problem than pollution and greenhouse gases. The human species is very adaptable, It can survive, perhaps somewhat diminished in population, with a goodly about of CO2 in the air.
It CANNOT survive when the water supply becomes such that one is deprived of the precious liquid for much over four days. That could happen.
Naf, retiree; mathematics; CompSci; Navy vet
Also perhaps of interest might be the Spanish-language curriculum unit on water, offered by the AP as a model for preparing students for the Spanish Language and Culture AP exam.  I have enjoyed teaching it.  http://pages.uoregon.edu/rldavis/agua.pdf
I write for a blog that covers the connections between food, water and energy and this is great. I’d love to see one of these for energy and food/agriculture. Big topics, for sure, but useful for a lot of teachers I’d guess.
Hi Robin — Great idea. We’ll put it on the ongoing list, and thanks for writing in! –Katherine
I find that many of the regions without enough water, for themselvs or their crops will use an irrigation method. So they can move water from one place and bring it to another.
Lesson plans across the curriculum based on Times content.
We invite students 13 and older to comment on issues in the news.
Our weekly newsletter features recent resources for teaching with The Times.
Advertisement
Go to Home Page »