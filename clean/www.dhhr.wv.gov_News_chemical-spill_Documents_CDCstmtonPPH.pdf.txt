The West Virginia Bureau of Public Health received the following statement in an 
email from the CDC tonight.  Testing is ongoing and State officials are continuing 
to work with CDC and other experts to ensure the safety of the water for our 
citizens. 

 

Earlier today, the manufacturer reported that another material was 
part of the chemical release that occurred on January 9, 2014. This 
material has been identified as a proprietary mixture of polyglycol 
ethers (PPH).  It was in the same tank and entered the water system at 
the same time as the MCHM.  PPH represented a relatively small 
percentage (approximately 5%) of the total volume in the tank. 

Toxicologic information on PPH is limited.  Based on the Material Safety 
Data Sheets (MSDS) provided by the manufacturer, the reported 
toxicity of this material appears to be lower than the toxicity of 
MCHM  (LD50 > 2000 mg/kg for the primary component of PPH vs. 825 
mg/kg for MCHM).  Given the small percentage of PPH in the tank and 
information suggesting similar water solubility as MCHM, it is likely that 
any amount of PPH currently in the water system would be extremely 
low. However, the water system has not been tested for this material. 

An initial review of the currently available toxicologic information does 
not suggest any new health concerns associated with the release of 
PPH.  At this point, toxicologic information about PPH is limited; 
however, CDC/ATSDR will continue to work closely with the State of 
West Virginia and its Federal partner agencies to search for additional 
relevant information.   

 

