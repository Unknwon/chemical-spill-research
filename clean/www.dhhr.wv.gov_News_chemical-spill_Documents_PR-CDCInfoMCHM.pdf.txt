 

 
 

 

 
 
 
 
 
 
 

 

 

 

 

 

 
CDC Media Relations 
404-639-3286  

West Virginia Joint Information Center 
westvirginiajic@wv.gov  
304-558-8055 

 
 

FOR IMMEDIATE RELEASE 

January 17, 2014 

 
CHARLESTON,  W.Va.  –  The  Centers  for  Disease  Control  and  Prevention  today  provided  the 
West  Virginia  Department  of  Health  and  Human  Resources  Bureau for  Public  Health  with the 
following information regarding 4-methylcyclohexanemethanol (MCHM), the chemical which has 
contaminated the water supply for residents in parts of 9 West Virginia counties.  
 
The information provided by the CDC is posted at 
http://emergency.cdc.gov/chemical/MCHM/westvirgina2014/index.asp 
 
 

### 

 

