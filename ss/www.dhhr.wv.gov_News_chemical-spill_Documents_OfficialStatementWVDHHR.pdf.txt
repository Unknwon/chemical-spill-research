 

 

 

Earl Ray Tomblin  

Governor  

DEPARTMENT OF HEALTH AND HUMAN RESOURCES 

STATE OF WEST VIRGINIA 

Office of the Secretary 

One Davis Square, Suite 100, East 
Charleston, West Virginia  25301 

Telephone: (304) 558-0684   Fax: (304) 558-1130 

 
 

January 13, 2014 

  

Karen L. Bowling  
Cabinet Secretary  

Statement from Karen Bowling, Cabinet Secretary,  

West Virginia Department of Health and Human Resources 

 

State and local health care officials have reviewed and concur with the guidelines 

developed by West Virginia American Water Company for flushing water systems to 

ensure water quality.
IMPORTANT: Please be advised you should only follow 

these guidelines when the “do not use” order has been 

lifted for your zone.
It’s critical that you adhere to the 

guidelines by zone to help avoid service interruption.
Thank you for your patience and cooperation during this crisis.
We are confident if we 

follow these guidelines by zone, will ensure the safe restoration of water to affected 

areas.
### 

