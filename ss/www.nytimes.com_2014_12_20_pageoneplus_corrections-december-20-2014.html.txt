Advertisement
Advertisement
DEC. 20, 2014
Advertisement
FRONT PAGE
An article on Oct. 24 about low television ratings for the early games of the World Series described incorrectly a catch made by Willie Mays in Game 1 of the 1954 Series.
Though he caught the ball as it came over his shoulder from behind, it was not an over-the-shoulder “basket catch.” (In Mays’s signature basket catch, the glove was held palm up and at belt level; Mays had his arms outstretched for that 1954 catch.)
This correction was delayed because a reader’s emails did not reach the editors.
•
An article on Friday about the potential effects of the détente with Cuba on relations between the United States and Latin America, which has long objected to Washington’s isolation of Cuba, misspelled the surname of a retired toy store owner in Buenos Aires who expressed skepticism about the deal, saying American leaders “must have a knife somewhere under their poncho.” He is Rubén Grimaldi, not Grimaldo.
NATIONAL
An article on Thursday about indictments in a West Virginia chemical spill misidentified the lawyer for William E. Tis and Charles E. Herzing, two of the men charged.
He is Steve Jory — not Matthew Benyon, who was the public relations consultant who released a statement by Mr. Jory.
NEW YORK
An article on Tuesday about a New York City high school student’s false claim to have earned millions of dollars trading stocks misstated, in some editions, the day that The New York Post featured the youth on its cover.
It was Sunday, not Monday.
•
Because of an editing error, an article in some editions on Dec. 12 about the acquittal of Robert A. Durst, a troubled member of a prominent New York City real estate family who was accused of trespassing, misstated the year of an earlier case in which he was found not guilty of murder in Texas.
It was 2003, not 2001.
BUSINESS DAY
A picture caption on Thursday with the State of the Art column, about Amazon.com’s vulnerability to technological advances, misidentified the manager of a courier fleet in San Francisco in some copies.
She is Ashlie Gudmundsen, not April Conyers.
•
An article on Wednesday about the appointment of a new chief executive for the retailer American Apparel after the earlier ousting of its founder and chief misstated part of the name of the firm that Craig Johnson, who commented on American Apparel’s comeback chances, serves as president.
It is Customer Growth Partners, not Consumer Growth Partners.
WEEKEND
An opera entry in the Listings pages on Friday about the Metropolitan Opera’s production of “Die Meistersinger von Nürnberg” misstated the time of today’s performance.
It is 6 p.m., not noon.
The entry also included outdated information about a live HD broadcast to theaters.
While last Saturday’s performance was broadcast, today’s will not be.
The entry also misidentified the singer who will perform the role of Hans Sachs.
He is the bass-baritone James Morris — not the baritone Michael Volle, who sang the role last Saturday.
•
A picture caption in the Listings pages on Friday about American Ballet Theater’s production of “The Nutcracker,” at the Brooklyn Academy of Music, misstated the starting time of one of the performances today.
There are performances at 2 and 7 p.m., not at 2 and 4:30 p.m.
Advertisement
Advertisement
ARTS & LEISURE
A cover article this weekend about the satirical movie “The Interview” contains an outdated reference to its release.
After the section had gone to press, Sony canceled the film’s Dec. 25 release because of terrorist threats made against theaters planning to show it.
The stars of the movie, about a plot to kill North Korea’s leader, Kim Jong-un, spoke to The New York Times before those threats were revealed.
OPINION
An Op-Ed article on Wednesday about the Sydney hostage siege misstated the timing of a statement by the gunman, Man Haron Monis: “This pen is my gun, and these words are my bullets.
I’ll fight with these weapons against oppression to promote peace.” He said that in 2009, when he was convicted of sending offensive letters to the families of soldiers killed in Afghanistan — not three days before the siege, when his appeal to overturn the conviction was rejected.
The Times welcomes comments and suggestions, or complaints about errors that warrant correction.
Messages on news coverage can be e-mailed to nytnews@nytimes.com or left toll-free at 1-888-NYT-NEWS (1-888-698-6397).
Comments on editorials may be e-mailed toletters@nytimes.com or faxed to (212) 556-3622.
Readers dissatisfied with a response or concerned about the paper’s journalistic integrity may reach the public editor at public@nytimes.comor (212) 556-7652.
For newspaper delivery questions: 1-800-NYTIMES (1-800-698-4637) or e-mail customercare@nytimes.com.
A version of this article appears in print on December 20, 2014, on page A2 of the National edition with the headline: Corrections.
Order Reprints| Today's Paper|Subscribe
Go to Home Page »