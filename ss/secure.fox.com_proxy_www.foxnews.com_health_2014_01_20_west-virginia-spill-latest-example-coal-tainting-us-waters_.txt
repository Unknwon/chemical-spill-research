Sign in to comment!
Water is distributed to residents at the South Charleston Community Center in Charleston, West Virginia, January 10, 2014.
(REUTERS/Lisa Hechesky)
The chemical spill that contaminated water for hundreds of thousands in West Virginia was only the latest and most high-profile case of coal sullying the nation's waters.
For decades, chemicals and waste from the coal industry have tainted hundreds of waterways and groundwater supplies, spoiling private wells, shutting down fishing and rendering streams virtually lifeless, according to an Associated Press analysis of federal environmental data.
But because these contaminants are released gradually and in some cases not tracked or regulated, they attract much less attention than a massive spill such as the recent one in West Virginia.
"I've made a career of body counts of dead fish and wildlife made that way from coal," said Dennis Lemly, a U.S. Forest Service research biologist who has spent decades chronicling the deformities pollution from coal mining has caused in fish.
"How many years and how many cases does it take before somebody will step up to the plate and say, `Wait a minute, we need to change this'?"
The spill of a coal-cleaning chemical into a river in Charleston, W.Va., left 300,000 people without water.
It exposed a potentially new and under-regulated risk to water from the coal industry when the federal government is still trying to close regulatory gaps that have contributed to coal's legacy of water pollution.
From coal mining to the waste created when coal is burned for electricity, pollutants associated with coal have contaminated waterways, wells and lakes with far more insidious and longer-lasting contaminants than the chemical that spilled out of a tank farm on the banks of the Elk River.
Chief among them are discharges from coal-fired power plants that alone are responsible for 50 percent to 60 percent of all toxic pollution entering the nation's water, according to the Environmental Protection Agency.
Thanks to even tougher air pollution regulations underway, more pollution from coal-fired power plants is expected to enter the nation's waterways, according to a recent EPA assessment.
"Clean coal means perhaps cleaner atmosphere, but dirtier water," said Avner Vengosh, a Duke University researcher who has monitored discharges from power plant waste ponds and landfills in North Carolina.
In that state, Vengosh and other researchers found contaminants from coal ash disposal sites threatening the drinking water for Charlotte, the nation's 17th-largest city, with cancer-causing arsenic.
"It is kind of a time bomb that can erupt in some kind of specific condition," Vengosh said.
The water shows no signs of arsenic contamination now.
In southeastern Ohio, tainted water draining from abandoned coal mines shuttered a century ago still turns portions of the Raccoon Creek orange with iron and coats the half-submerged rocks along its path white with aluminum.
Public drinking water systems in 14 West Virginia counties where mining companies are blasting off mountaintops to get to coal seams exceeded state safe drinking water standards seven times more than in nonmining counties, according to a study published in a water quality journal in 2012.
The systems provided water for more than a million people.
The water quality monitoring in mining areas is so inadequate that most health violations likely were not caught, said Michael Hendryx, the study's author and a professor of applied health at Indiana University.
The EPA, in an environmental assessment last year, identified 132 cases where coal-fired power plant waste has damaged rivers, streams and lakes, and 123 where it has tainted underground water sources, in many cases legally, officials said.
Among them is the massive failure of a waste pond at a Tennessee Valley Authority power plant in 2008.
More than 5 million cubic yards of ash poured into a river and spoiled hundreds of acres in a community 35 miles west of Knoxville.
Overall, power plants contributed to the degradation of 399 bodies of water that are drinking water sources, according to the EPA.
There are no federal limits on the vast majority of chemicals that power plants pipe directly into rivers, streams and reservoirs.
The EPA just last year proposed setting limits on a few of the compounds, the first update since 1982.
More than five years after the Tennessee spill, the EPA has yet to issue federal regulations governing the disposal of coal ash.
Experts say the agency is playing catch-up to solve a problem that began when it required power plants in the 1990s to scrub their air pollution to remove sulfur dioxide.
An unintended consequence was that the pollutants captured were dumped into landfills and ponds, many unlined, where they seeped into underground aquifers or were piped into adjacent rivers, reservoirs and lakes.
"As you are pushing air rules that are definitely needed, you need to think of the water.
And they didn't," said Eric Schaeffer, a former EPA enforcement official.
"Now they are running after the problem."
He now heads the Environmental Integrity Project, a group whose research has uncovered previously unknown sites of contamination from power plant waste pits.
The federal government has in recent years issued the first-ever regulations for mercury released from power plant smokestacks, the largest source of mercury entering waterways.
The EPA has stepped up its review of mountaintop mining permits, to reduce pollution.
"Coal-related pollution remains a significant contributor to water quality pollution across the United States," said Alisha Johnson, an EPA spokeswoman.
"The EPA's efforts have yielded significant improvements, but significant work still remains."
On the mining side, a review of federal environmental enforcement records shows that nearly three-quarters of the 1,727 coal mines listed haven't been inspected in the past five years to see if they are obeying water pollution laws.
Also, 13 percent of the fossil-fuel fired power plants are not complying with the Clean Water Act.
Many mines don't even report their discharges of selenium, although researchers have found the chemical near mines at levels where it can cause deformities and reproductive failure in fish.
A study in the journal Science in 2010 found that 73 of 78 West Virginia streams in mountaintop mine removal areas had selenium levels higher than the official threshold for fish life.
Higher levels of selenium - a natural component of coal that seeps from rock when water runs through it - often means fish don't reproduce or have deformed, even two-headed, offspring, Lemly said.
University of Maryland environmental sciences professor Margaret Palmer spent much of the weekend that Charleston was without water testing the Stillhouse Branch stream near Clay, W.Va., just below a mountaintop removal coal mine.
She said her tests showed the water was too salty from the rocks from the mine.
"It's like a desert with a few water rats in it," Palmer said.
"The organisms that do live in (these streams), you think of them like water rats.
Only the really hearty ones survive."
Efforts by the EPA to ease the problem, by requiring mine permits to be judged by a measure of the saltiness in downstream water, have been vacated by a federal court.
That decision is under appeal.
A spokesman for the National Mining Association said the industry operates in accord with extensive and rigorous permitting guidelines.
Pollution still enters the environment from coal mined decades ago.
The EPA estimates 12,000 river miles are tainted by acid mine drainage from long-shuttered coal mines.
One of them is Raccoon Creek in southeastern Ohio.
"These mines have been abandoned for a hundred years," said Amy Mackey, Raccoon Creek's watershed coordinator.
"There is no one to fall back on."
States take the lead on the water pollution front.
But advocacy groups from at least three states in coal country - Kentucky, West Virginia and Indiana - have asked the EPA to step in, arguing that state officials aren't doing enough.
Advertisement
More
Weekly news and features that matter the most to your well-being
Advertisement
This material may not be published, broadcast, rewritten, or redistributed.
©2015 FOX News Network, LLC.
All rights reserved.
All market data delayed 20 minutes.
Privacy - Terms - FAQ