
Tonight, the fourth season of Project Runway: All Stars comes to an end and host Alyssa Milano says it is a “pressure cooker” of emotion.
Yet, all the stress wasn’t only on Dmitry Sholokhov, Helen Castillo, and Sonjia Williams, the final three contestants tasked with designing a collection in just four days.
The reality competition host reveals it was also stressful for the judges, who would go through two stages of emotions each week -- 1) Anxiety over sending the right person home, 2) Excitement and anticipation for the next runway -- as they determined who will win.
“[It was full of] nerves and anxiety,” Milano tells ETonline about the feeling in the air during the final runway.
NEWS: Alyssa Milano Welcomes Baby Girl
But things were a little more emotionally-charged and physically painful for Milano, who gave birth to her second child just three weeks after taping ended.
“I had contractions throughout the entire finale,” she admits adding that things were “a lot more weepy.”
“Our showrunners were constantly telling me, ‘You have to be strong.
Stop crying,’” Milano says, “because they wanted me to be a stoic judge.” And the host fully admits that she wasn’t able to put her emotions aside, even revealing that she cried for 20 minutes after one competitor was eliminated.
“They had to cut the camera.”
NEWS: Alyssa Milano Reacts to Controversy Over Breastfeeding Selfie
While being pregnant and working up through the end of the third trimester is a feat in itself, Milano jokes that the most difficult thing was finding “shoes that didn’t have ankle straps.”
NEWS: Alyssa Milano Questions Why Her Photo Is More Offensive Than Kim Kardashian's
And as for tonight’s finale, she promises it is one to watch.
Project Runway: All Stars airs on Lifetime tonight at 9 p.m.
ET.
Alyssa Milano

By checking this box you acknowledge our Privacy Policy and agree to the Terms of Use.
THANK YOU
What a Real Dominatrix Thinks 'Fifty Shades of Grey' Got Right – And Way Wrong
Kendall Jenner Is a Bombshell in 'Allure,' But Admits She Didn't Always Look This Good
Entertainment News | Celebrity News | Entertainment Tonight
7 Things That Surprised Us Watching 'Fifty Shades of Grey' (Including Jamie Dornan’s Penis!)
Jussie Smollett Debuts His Own Music on 'Empire': 'These Are My Stories'
Channing Tatum and George Clooney's Hacked Sony Emails Will Make You Like Them Even More

By checking this box you acknowledge our Privacy Policy and agree to the Terms of Use.
THANK YOU
8:00 PM
Grandfathered
8:30 PM
The Grinder
9:00 PM
Rosewood
10:00 PM
Fox 22 News at 10
11:00 PM
Animation Domination High-Def
8:00 PM
College Football: Penn State at Ohio State
11:30 PM
Leverage
Closed Captioning | FCC Public File | EEO Reports
Disclaimer: All information deemed reliable but not guaranteed and should be independently verified.
Neither Rockfleet Broadcasting III LLC / Bangor Communications LLC.
nor Links Online Marketing LLC shall be responsible for any typographical errors, misinformation, or misprints.