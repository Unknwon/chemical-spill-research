Advertisement
Advertisement
By THE EDITORIAL BOARDJAN.
16, 2014
Advertisement
The chemical spill that cut off water to more than 300,000 people in West Virginia for several days has exposed serious defects in state and federal environmental protections that allow many facilities and chemicals to escape scrutiny.
Investigators are still trying to figure out exactly how an estimated 7,500 gallons of a chemical used to clean coal called 4-methylcyclohexane methanol, or MCHM, leaked from a storage facility into the Elk River.
But state and federal agencies clearly should have done more to limit the risks.
For starters, the state failed to adequately inspect how the facility stored chemicals, though it did send inspectors there to check on air quality.
The chemicals were kept in tanks on the riverbank, upstream from a large water-treatment plant that supplies Charleston.
The spill is the third major chemical accident in the region in five years.
State lawmakers and regulators in West Virginia have a long history of coddling the coal and chemical industries, which dominate the state’s economy.
According to a 2009 investigation by The Times, companies that pollute state waters are rarely fined.
And state officials have so far ignored a 2011 proposal from the federal Chemical Safety Board urging new rules to prevent industrial accidents and spills.
That recommendation came after an explosion at a chemical plant near Charleston that killed two people in 2008.
The federal government also has a checkered record on chemical safety.
The main law regulating chemicals, the Toxic Substances Control Act of 1976, has allowed tens of thousands of inadequately tested chemicals, including MCHM, to remain in use.
(Experts say it’s unclear how harmful MCHM is and how much exposure could lead to death or a serious illness because the company that makes it has not publicly disclosed detailed information about the chemical.)
Instead of requiring manufacturers to show that their products are safe before they can be used, the law puts the burden of proof on the Environmental Protection Agency — a huge investigative and regulatory undertaking.
The result is that the E.P.A.
has tested just 200 of the roughly 85,000 chemicals in use today, and restricted fewer than a dozen.
What’s needed is meaningful reform like the Safe Chemicals Act of 2013 introduced by Senator Frank Lautenberg, Democrat of New Jersey, and Senator Kirsten Gillibrand, the New York Democrat, that would require manufacturers to prove that chemicals are safe before they can be sold.
In recent days, concentrations of MCHM in the water system have fallen sharply and some in the Charleston area can now drink the tap water.
But the passing of this crisis should not dissuade the state or the federal government from strengthening and enforcing statutes.
Meet The New York Times’s Editorial Board »
A version of this editorial appears in print on January 17, 2014, on page A24 of the New York edition with the headline: Contaminated Water in West Virginia.
Today's Paper|Subscribe
Go to Home Page »