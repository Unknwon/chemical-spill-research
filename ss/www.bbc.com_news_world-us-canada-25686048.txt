
About 300,000 people in the US state of West Virginia have been warned not to drink tap water after a chemical spill into a river near the state capital.
The water advisory closed schools and businesses in nine counties, and cancelled a state legislative session.
A foaming agent used in the coal preparation process leaked from a tank at Freedom Industries on Thursday.
Officials said it was unclear how dangerous the spill was but they declared a water advisory to be safe.
President Barack Obama has accepted West Virginia's request for a disaster declaration, which allows federal aid to be used.
Jeff McIntyre, president of West Virginia American Water, told the Associated Press news agency: "Until we get out and flush the actual system and do more testing, we can't say how long this will last at this time.
Charleston Mayor Danny Jones said at a press conference on Friday the spill had been "devastating to the public at large and to the people that live in our city".
Freedom Industries has said the chemical, 4-methylcyclohexane methanol, could be harmful if swallowed and cause skin and eye irritation.
In a statement, the firm said it was working with local and federal officials and "following all necessary steps to fix the issue".
The West Virginia National Guard planned on Friday to deliver bottled water to emergency services.
At a store in Charleston, the state capital, a police officer stood guard as shoppers stocked up on bottled water on Thursday evening, according to the Charleston Gazette newspaper.
"People have been grabbing it like crazy," Kerstin Halstead told the Gazette.
"Some people were getting - well, they could have shared more."
The spill was first found after the state's environmental protection department received a report of a strange odour near the Elk river on Thursday.
Officials found a leaking storage unit, a spokesman said.
The spill had overrun a containment area and leaked into the river.
While the leaking container held at least 40,000 gallons (182,000 litres), state environmental official Tom Aluise said they were "confident" that no more than 5,000 gallons escaped.
The BBC is not responsible for the content of external Internet sites
Slovenia's army will help police deal with thousands of migrants expected to arrive from Croatia in coming days, PM Miro Cerar announces.
Why did this man keep hold of a quilt for 70 years?
Inside Iran's secretive revolutionary courts
What music sounds like in the Great White North
Target stores attacked by pornographic pranksters
When polar bears come to town
Why did a US woman sue her 12-year-old nephew?
Why the Modi-Zuckerberg hug is causing concern in India
Take a look inside car maker Ford's hi-tech 'mega factory'
A black woman's foul-mouthed anti-Muslim tirade on a London bus