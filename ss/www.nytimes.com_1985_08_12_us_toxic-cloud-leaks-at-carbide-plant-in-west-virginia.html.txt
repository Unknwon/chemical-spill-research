Want both newspaper
deliveryand free, unlimited digital access?
Want unlimited access
to NYTimes.com and our apps?
CHARLESTON, W.Va., Aug. 11— 
                    A small cloud of toxic chemicals escaped from a Union Carbide plant near here this morning, and at least 135 residents were treated for eye, throat and lung irritation.
Twenty-eight of the injured were admitted to hospitals here..
The chemical that escaped today was first identified as aldicarb oxime.
Tonight, however, company officials said aldicarb oxime was only one constituent of the cloud of gas that also contained several other chemicals.
Aldicarb oxime is combined with methyl isocyanate, or MIC to produce aldicarb, a compound used in pesticides.
The Institute plant manufactures aldicarb and ships it to another facility.
MIC Leak Is Denied
Methyl isocyanate is the gas that escaped from a Union Carbide plant in Bhopal, India, last December, killing 2,000 people.
Union Carbide officials said today that no MIC escaped from the plant today.
Dr. Jack Tolliver, chief of emergency medicine at the Charleston Area Medical Center, said Dr. Bipin Avashia, the Union Carbideplant physician, told him that  the compound was ''a very minor irritant'' that would have ''no long-term effects.''
Institute, a town of 3,100 people that largely surrounds West Virginia State  College, is about 10 miles north of Charleston.
The Kanawha County emergency dispatcher's office said a number of telephone calls were received tonight from homeowners who said they could smell the gas again and were fearful that a new leak had occurred.
But the dispatcher's office said the odors were the result of ''some of the stuff that lingered, condensing  in the cooler evening air and coming down in low-lying places.''
''We have it definitely confirmed that there is no new leak,'' said Mitchell  Lewis, a county ambulance dispatcher.
''We had 10 to 15 calls between 9:30 and 10 P.M., but no one was transported to a hospital.
We are telling people that there is no need to evacuate but that people with respiratory problems should stay indoors and turn off air conditioners.''
Other Chemicals Listed
Thad Epps, a spokesman for the company, said aldicarb oxime was significantly less toxic than MIC.
Dick Henderson, another company spokesman, said aldicarb oxime should not contain any MIC.
''Our best preliminary estimate is that no MIC escaped.''
he said.
''The initial surmise was that aldicarb oxime containing some very small parts of MIC was what was involved.''
''Since then,'' Mr. Henderson said, ''we have modified our estimate to include aldicarb oxime; dichloromethane, a solvent used to contain the oxime; carbon monoxide; carbon dioxide and some sulfur compounds, which were responsible for the strong ordor problems that came along with this.
Obviously that is less toxic than MIC.''
''On a scale of 1 to 5,'' he went on, ''oxime is a 1 - at the least toxic end of the scale.
It tends to make you nauseous, that's what it does.
It is toxic only in very large quantities.''
He said that the leak, which occurred when increasing pressure burst a gasket in an aldicarb oxime storage tank, had not interrupted the production of aldicarb.
The plantat Institute is one of the few facilities in the world still producing MIC and has drawn much scrutiny since the Bhopal incident.
Today marked the first time toxic chemicals had leaked from the plant into surrounding communities in the Kanawha Valley.
$5 Million Warning System Union Carbide closed the Institute plant in December and spent more than $5 million over five months on renovations that the company said would prevent an accident like  the one in India.
The plant's new system, called Safer, is designed to pinpoint the speed and direction of any airborne chemical leak.
Computers then warn plant operators, who are supposed to sound the siren immediately and notify local emergency officials.
But nearby residents said today that they smelled the pungent chemical and felt its burning effects before they heard emergency horns, which alerted them to check for warnings on radio and television.
Clifford Cyrus, who lives 500 yards from the plant, told The Associated Press that he and his wife, Barbara, smelled the gas but heard no siren.
''I didn't know what to do,'' he said.
''About 10 minutes later we heard the whistle and then we headed toward Charleston.''
A 15-Minute Leak
A company statement said the leak began about 9:35 when a gasket failed on a  500-gallon storage tank.
Mr. Henderson said the siren was sounded by 9:40 and the leak stopped after 15 minutes.
However residents and some golfers and tennis players at a nearby country club said they saw the plume of gas rising slowly from the plant about 9:15.
A cream-colored cloud spread slowly into Dunbar, Nitro, St. Albans and South  Charleston, neighboring communities along the banks of the Kanawha River, according to residents who first spotted the leak.
A four-member technical team from the Environmental Protection Agency, with sophisticated monitoring equipment, arrived at Institute today, according to David Cohen, an agency spokesman in Washington.
He said a five-member investigative team was also being sent to Institute.
#6 Workers Were Injured David Seidler, vice chief of emergency services at the Charleston Area Medical Center, said six workers at the plant had suffered ''severe eye irritations'' and were in stable or satisfactory condition.
Although broadcasts warned residents to stay inside and turn off air conditioning and other ventilation, many people treated at five local hospitals  and a makeshift emergency center said the chemical had accumulated indoors.
The accident today angered many people who live close to plants operated by Union Carbide and other chemical companies.
Iris Bell, who lives about a mile from Institute, said she thought the Kanawha Valley, a chemical manufacturing center, was no longer a safe place to live.
''This stuff makes my head ache and my skin itch,'' Mrs. Bell said.
''It also makes me really scared about how they let these things out.
I guess it means we  all have to move out of here if we want to be safe.''
Union Carbide reported in January that small amounts of MIC had leaked within the Institute plant on 61 occasions beginning in 1980.
When Union Carbide reopened its MIC unit in May, it decided not to ship MIC to its affiliates, but instead to ship the less toxic aldicarb, which is made from aldicarb oxime and MIC.
Company officials said aldicarb oxime is used to manufacture the pesticide Temik, which was reformulated to avoid the use of MIC this spring.
Union Carbide has made aldicarb oxime in Institute for years.
It transports it to another of its plants in Woodbine, Ga., where it is processed into Temik.
photo of Donna Willis and her son being examined by Glen Acker (AP)(page A12); map of West Virginia highlighting Charleston (page A12); photo of paramedics moving patients to emergency medical center (AP)
Room for Debate asks whether shorefront homeowners should have to open their land to all comers.
New York City’s top public schools must become more diverse.