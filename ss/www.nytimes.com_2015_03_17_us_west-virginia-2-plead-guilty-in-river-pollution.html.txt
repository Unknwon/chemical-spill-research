Advertisement
Advertisement
By THE ASSOCIATED PRESSMARCH 16, 2015
Advertisement
Two former owners of Freedom Industries pleaded guilty on Monday to environmental violations stemming from last year’s Charleston chemical spill, which prompted a temporary tap water ban for 300,000 residents.
At separate hearings, William Tis, 60, and Charles Herzing, 64, entered the pleas to causing an unlawful discharge of a coal-cleaning agent into the Elk River.
Each faces up to a year in prison.
They also face fines of $25,000 per day per violation or $100,000, whichever is greater.
But after entering his plea, Mr. Tis expressed doubt when Judge Thomas Johnston of Federal District Court asked him whether he had committed the crime.
“I have signed my name to these documents,” Mr. Tis said.
“No, I don’t believe I have committed a crime, but I am pleading guilty.” Pressed by the judge to explain, he said: “I do believe I am guilty of this offense.
There are people we had hired,” and “their failure results in my failure.” Plea hearings are scheduled Wednesday for a consultant and the tank farm plant manager, and next Monday for the company itself.
The former owner, Dennis Farrell, and former president, Gary Southern, face trial this year on charges related to the spill.
Freedom filed for bankruptcy protection eight days after the Jan. 9, 2014, leak.
A version of this brief appears in print on March 17, 2015, on page A17 of the New York edition with the headline: West Virginia: 2 Plead Guilty in River Pollution.
Order Reprints| Today's Paper|Subscribe
Go to Home Page »