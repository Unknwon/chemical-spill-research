Advertisement
Advertisement
By TRIP GABRIEL and CORAL DAVENPORTJAN.
13, 2014
Advertisement
CHARLESTON, W.Va. —  The accidents kept coming, and so did the calls for a plan to improve West Virginia’s chemical safety regulations.
Last week’s massive chemical spill into West Virginia’s Elk River was the region’s third major chemical accident in five years.
It came after two investigations by the federal Chemical Safety Board in the Kanawha Valley, also known dryly as Chemical Valley.
And it came on the heels of repeated recommendations from federal regulators and a local environmental advocacy group that the state adopt rules embraced in other communities to safeguard chemicals.
All of those recommendations died a quiet death with barely any consideration by state and local lawmakers, federal regulators and local environmental groups said.
“We are so desperate for jobs in West Virginia, we don’t want to do anything that pushes industry out,” said Maya Nye, president of People Concerned About Chemical Safety, a citizens group that formed after a 2008 explosion and fire that killed two workers at the Bayer CropScience plant in Institute, W.Va.
State officials on Monday began lifting a ban on using tap water, starting with hospitals and extending slowly by zones to the 300,000 people who have been without drinking water since late Thursday.
“The numbers we have today look good, and we’re finally at a point where the do-not-use order is being lifted in certain areas,” Gov.
Earl Ray Tomblin announced at a midday news conference.
Environmental and health officials monitoring the concentration of the chemical in the water system, known as MCHM, said the concentration had dropped consistently below one part per million, a safety threshold set by the Centers for Disease Control and Prevention and the Environmental Protection Agency.
A web page was set up to alert residents when they could begin to flush their home plumbing systems, first by running the hot water for 15 minutes, then the cold for five minutes, to remove tainted residue in pipes and water heaters.
“I just took a shower, the first one in five days, and it feels pretty good,” said Charles Matheny, 37, standing in front of his building in downtown Charleston, the first neighborhood to be given the all-clear.
But the return to normalcy comes amid renewed attention to the region’s troubled environmental safety record, and what some say is indifference to addressing it.
Booth Goodwin, the United States attorney for southern West Virginia, whose office is investigating whether federal laws were broken, said the easing of the crisis should not be the end of the story.
77
3 Miles
Elk River
KANAWHA COUNTY
Site of spill
Charleston
64
Kanawha
River
South Charleston
Kanawha
City
Area where drinking
water ban was lifted
as of 7 p.m. Mon.
Kanawha
River
Source: West Virginia American Water
“Our drinking water is not something you can take chances with, and this mess can never be allowed to happen again,” he said.
After the 2008 Bayer CropScience explosion, Ms. Nye and other citizens drafted a plan based on one used in Contra Costa County, Calif., which experts say has some of the best chemical oversight in the country.
In 2010, the federal Chemical Safety Board completed its investigation into the Bayer CropScience explosion, and its report included a recommendation that West Virginia adopt the proposal of Ms. Nye’s group, which had presented it to the Legislature in 2009.
Advertisement
Advertisement
In 2010, the release of a toxic gas from a DuPont plant in Belle, W.Va, caused the death of another worker.
The Chemical Safety Board returned, and again it said new regulations were essential.
“All those investigations led to recommendations to increase state and federal oversight,” said Daniel Horowitz, managing director of the Chemical Safety Board.
The board recommended creating a safety program to be headed by Dr. Rahul Gupta, the executive director for the Kanawha-Charleston Health Department.
Dr. Gupta was eager to lead the program, but he said the state government had no interest.
Jeffrey V. Kessler, the president of the West Virginia Senate, said the proposals never found a champion in the Legislature.
“No one grabbed the lead and said we need to implement these,” he said.
Now, he said, lawmakers are looking at tightening regulations to require early notification of a spill, greater setbacks from water supplies for chemicals, and better alarm systems.
“People always beat the drum about too much government regulation,” he said.
“My goodness, there are 300,000 people I guarantee wish they had a little more regulation.”
Freedom Industries, the owner of the tank on the bank of the Elk River that leaked into the public water system, was little known before the spill.
Founded in 1992, according to state filings, it merged at the end of last year with three other companies, including Etowah River Terminal, the site of the tank farm.
Gary Southern, the company president, spoke briefly to reporters on Friday night, apologizing to the public, but has not commented since.
By Monday afternoon, West Virginia American Water, the regional supplier, said it had lifted the ban for 10,000 customers in downtown Charleston and the Kanawha City neighborhood.
Molly Bowen, who was sucking a lollipop, uttered a wish unusual for a 9-year-old: “I want to bathe really bad.”
Debbie Weinstein, the executive director of the local Y.W.C.A., which runs a shelter for 100 homeless women and children, said she was feeling “the greatest sense of relief” that residents would be able to shower.
At the same time, many residents expressed a wariness over the safety of their water now.
Since the leak was first detected by citizens aware of its licorice odor — and not by either the storage company or the water supplier — people speculated about what might have happened if there had been no smell.
Mr. Kessler, the State Senate president, said he did not feel ready to imbibe just because MCHM measurements were under one part per million.
“I want it to be zero for several days and then I’ll feel better,” he said.
For now, he said, he was sticking with bottled water.
Residents in areas where the ban remained in effect were philosophical, knowing the end was in sight.
At the Harvest Time Church of God, which shelters about 25 men, Jess Inclenrock, the pastor, said they were making do without showers.
“We just keep our arms down,” he said.
“When we worship, we keep our hands below our shoulders, you know.” He mimicked the gesture of waving his open hands without raising them above his shoulders.
“Hallelujah.”
Trip Gabriel reported from Charleston, and Coral Davenport from Washington.
Daniel Heyman contributed reporting from Charleston, and Jack Begg from New York.
A version of this article appears in print on January 14, 2014, on page A12 of the New York edition with the headline: Calls for Oversight in West Virginia Went Unheeded.
Order Reprints| Today's Paper|Subscribe
Go to Home Page »